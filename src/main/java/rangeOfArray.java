import java.util.Scanner;

public class rangeOfArray {
    public static void main(String[] args) {
        int n, i, min = 99999, max = -7777;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        int arr[] = new int[n];
        for (i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
            if (arr[i] > max)
                max = arr[i];
            if (arr[i] < min)
                min = arr[i];
        }
        int range = max - min;
        System.out.println(range);
    }
}
